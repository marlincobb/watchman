//
// --> Watchman is a product of Fortunesrocks.me LLC
// --> Purpose : Administor saftey / compliance / security for FortunesRocks.me servers
//
//
var log4js = require('log4js');
log4js.replaceConsole();
L = log4js.getLogger();
version = "2.00";
L.info("===================================================");
L.info("====>> Watchman Version:",version," Started <<========");
L.info("===================================================");
var fs = require('fs')
var mongoose = require('mongoose')


emailAlerts = require ("./app/emailAlert.js")();
emailAlerts.init();

w = require("./app/websites.js")();
websites = w.getListFromFile();

clam = require("./app/clamscan.js")(L);
clam.exempt = [];
clam.exempt.push("/ninjafirewall/lib/share/sigs.txt: SecuriteInfo.com.JS.Exploit-16.UNOFFICIAL FOUND");
fmeAV = require("./app/fmeAV.js")(L);

//
// Start the Clam filters
//
var clamFilter = true;
var clami = 0;
if (clamFilter) {
	L.info("Refreshing CLAM database")
	clam.refresh(function() {
		L.info("CLAM: Database Refreshed, beginning Clamscan");
		var clami = 0;
		var sitePath = websites[clami].root;
		scanClam(clami)
	});
}
	
function scanClam (clami) {
	function exempt(clami,bad) {
		var exemptFiles = websites[clami].clam.exempt;
		L.info("scanClam:exempt",exemptFiles);
		if (exemptFiles) {
			for (var i in exemptFiles) {
				if (bad.indexOf(exemptFiles[i]) > -1 ) return true
			}
		}
		return false;
	}
	
	L.info("Clam: Scanning ",websites[clami].root)
	clam.scanDirectory(websites[clami].root,function(hasVirus,badFiles){ 
		// L.info("Clam: finished scanning ",websites[clami].site)
		if (hasVirus) {
			L.error("avScanSite found the following number of viruses",badFiles.length);
			for (var index in badFiles) {
				var bad = badFiles[index];
				if (exempt(clami,bad)) badFiles.splice(index,1); 
				
			}
			if (badFiles.length == 0) {
				L.info("Clam: found an exmempt file so keep on going");
				
			} else {
				L.error("clam.scanDirectory:",websites[clami].site,"has a virus[es]",badFiles);
			//	emailAlerts.send("Watchman Clam Error:",JSON.stringify(badFiles));
				emailAlerts.template("watchmanErrors.jade","ClamScan AV Error: "+websites[clami].site,"ClamAV Scan:",websites[clami].site,badFiles)

			}
		} else {
			// L.info("Site",websites[clami].site,"no virus found");
		}
		clami++;
		if (clami < websites.length) {
			sitePath = websites[clami].root;
			scanClam(clami);

		} else {
			L.info("**** Watchman.avScanSite completed, restarting ****");
			L.info("Refreshing CLAM database")
			clam.refresh(function() {
				L.info("CLAM: Database Refreshed, beginning Clamscan");
				var clami = 0;
				var sitePath = websites[clami].root;
				scanClam(clami)
			});
			//scanClam(clami);
		}
		return;
	});
	
}
//
// ==> Globals Filter
//
	var globalFilter = true;
	globali = 0;
	if (globalFilter) {
		L.info("Globals: Starting scans")
		scanGlobal(globali);
	}
	function scanGlobal(globali) {
		var sitePath = websites[globali].root;
		L.info("Globals: scannaing => ",sitePath);
		fmeAV.scanGlobals(sitePath,function(errors) {
			if (errors.length > 0) {
				L.error("ScanGlobals Returned errors:",errors);
				emailAlerts.template("globalErrors",website[globali],errors);
			}
			globali++;
			if (globali > websites.length-1){
				globali = 0;
				L.info("#### GlobalFilter: restarting #####")
			}
			
			scanGlobal(globali)
		})
	}
//
// ==> permissions filters, checking filestructure ownership and permissions
//	
var wpPermissions = require("./app/filters/wpPermissions.js")();
var permissionFilter = true; 
wcount = 0;
if (permissionFilter) check(websites,wcount);
function check(websites,wcount) {	
	L.info("wpCheckOwner:",websites[wcount].site);
	var website = websites[wcount];
	wpPermissions.checkOwner(website,function(errors) {
		//L.info("wpPermissions returned",errors)
		// L.info (website.site,"returned the following");
		if (errors.length > 0)
			emailAlerts.template("watchmanErrors.jade","checkOwner Error: "+website.site,"wpPermissions.checkOwner",website,errors)
		wcount++;
		if (wcount < websites.length-1) {
			check(websites,wcount);
			return
		}	
		L.info("******** wpPermissions check owner complete **********");
		wcount = 0;
		checkP(websites,wcount);
	})
}

function checkP(websites,wcount) {	
	L.info("wpPermissions:",websites[wcount].site,wcount);
	var website = websites[wcount];
	wpPermissions.checkPermissions(website,function(errors) {
		// L.info (website.site,"returned the following");
		if (errors.length > 0)
			emailAlerts.template("watchmanErrors.jade","CheckPermissions Error: "+website.site,"wpPermissions.checkPermissions",website,errors)
		wcount++;
		if (wcount < websites.length) {
			checkP(websites,wcount);
			return
		}
		wcount = 0;
		check(websites,wcount);
		L.info("**************** wpPermissions:permissions has completed ***************");
		return;
	});
}

//
// ==> Start Watches on sites that have finals under survallence
//
surveillanceFitler = true; 
if (surveillanceFitler == true) {
	var surveillance = require("./app/filters/surveillance.js") ();
	L.info("*** watchma: Surveillance fiter starting *********");
	for (var w in websites) {
		var website = websites[w];
		if (website.surveillance) {
			if (website.surveillance.length) {
				for (var f in website.surveillance) {
					var file =  website.surveillance[f].watch;
					var clean = website.surveillance[f].clean;
					L.info("Setting up a watch for",file,website.site);
					watchFile(file,clean,website);
				}
			}
		}
	}
}
function watchFile(file,clean,website) {
	surveillance.watch(file,clean,website,function(file,clean,website) {
		L.error("watchman:surveillance had detected a file change",file,website.site);
		errors = ["A file: "+file+" underwatch has been changed"]
		emailAlerts.template("watchmanErrors.jade","Surveillance Error: "+website.site,"Surveillance Filter",website,errors);
		surveillance.clean(file,clean,website,function(file,clean,website) {
			L.info("restarting a watch",file);
			watchFile(file,clean,website);
		});// this is a sync function;
	
	})
} 
//
// ==> comparing website to standard wordpress reload
//
wordpressCompare = true; 
if (wordpressCompare) {
	var wpCompare = require ("/home/nodeapps/watchman/app/filters/diffToLatestWP.js")();
	runWPCompareSites();
	setInterval(runWPCompareSites,15*60*1000);
}
function runWPCompareSites() {
	for (var w in websites) {
		L.info("wpCompareSites:",websites[w].site);
		var website = websites[w];
		if (website.type == "wordpress" ) {
			wpCompare.scanWordPressSite(website,function(errors) {
				if (errors.length > 0) {
					emailAlerts.template("watchmanErrors.jade","Wordpress Compare Error: "+website.site,"WP Compare Error",website,errors);
					L.error("WPCompare:",errors);
				}
			})
		}
	}
}
/*
	//
// ===> Clean filters
//
	var cleanFilter = true;
	cleani = 0;
	if (cleanFilter) {
		L.info("Clean: filter started");
		scanClean(cleani);
	}
	function scanClean(cleani) {
		var ws = 
	}
*/
	