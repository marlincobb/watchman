[ 
	{
		site:"mrtopstep.com",
		type:"wordpress",
		database: "mts",
		databaseUser: "mts",
		databasePassword:"g2g1793",
		root:"/var/www/mrtopstep_com/wordpress/",
		contacts:"watch@fortunesrocks.me",
		ownerEmail:"info@mrtopstep.com",
		ownerName:"Bob Schillaci",
		watchFiles : [
			{
				watchFile:'/var/www/mrtopstep_com/wordpress/wp-content/themes/newsanchor_mts/header.php',
				cleanFile:'/root/sites/mrtopstep_com/mrtopstep_com/wordpress/wp-content/themes/newsanchor_mts/header.php'
			}
		],	
		clam: {
			exempt : [
				"/ninjafirewall/lib/share/sigs.txt: SecuriteInfo.com.JS.Exploit-16.UNOFFICIAL FOUND"
			]
		},
		noPHP : [
			{ 
				path: "wp-content/uploads",
				action: "delete",
				exempt: []
			}
		],
		clean : [
			{
				compareRoot: "/var/www/clean/wordpress/",
				path: "./",
				action: "replace",
				recurse: false,
				exempt: []
			},
			{
				compareRoot: "/var/www/clean/wordpress/",
				path: "wp-admin/",
				action: "replace",
				recurse: true,
				exempt: []
			},
			{
				compareRoot: "/var/www/clean/wordpress/",
				path: "wp-includes/",
				action: "replace",
				recurse: true,
				exempt: []
			}
			
			
		]
	}
]
	{
		site:"structuraltrading.com",
		type:"wordpress",
		database: "structuraltrading_com",
		databaseUser: "structural",
		databasePassword:"g2g1793",
		root:"/var/www/structuraltrading_com/wordpress",
		contacts:"watch@fortunesrocks.me",
		ownerEmail:"kathy@structuraltrading.com",
		ownerName:"Kathy Garber",
		watchFiles : [
			{
				watchFile:'/var/www/structuraltrading_com/wordpress/wp-content/themes/structuraltrading/header.php',
				cleanFile:'/root/sites/structuraltrading_com/wp-content/themes/structuraltrading/header.php'
			}
		],
		clam: {
			exempt : [
				"/ninjafirewall/lib/share/sigs.txt: SecuriteInfo.com.JS.Exploit-16.UNOFFICIAL FOUND"
			]
		},
		noPHP : [
			{ 
				path: "wp-content/uploads",
				action: "delete",
				exempt: []
			}
		],
		clean : [
			{
				compareRoot: "/var/www/clean/wordpress/",
				path: "./",
				action: "replace",
				recurse: false,
				exempt: []
			},
			{
				compareRoot: "/var/www/clean/wordpress/",
				path: "wp-admin/",
				action: "replace",
				recurse: true,
				exempt: []
			},
			{
				compareRoot: "/var/www/clean/wordpress/",
				path: "wp-includes/",
				action: "replace",
				recurse: true,
				exempt: []
			}
		]		
	},
	{
		site:"closingimbalance.com",
		type:"wordpress",
		database: "mim",
		databaseUser: "mim",
		databasePassword:"g2g1793",
		root:"/var/www/closingimbalance_com/wordpress",
		contacts:"watch@fortunesrocks.me",
		ownerEmail:"info@mrtopstep.com",
		ownerName:"Bob Schillaci",
		watchFiles : [
			{
				watchFile:'/var/www/closingimbalance_com/wordpress/wp-content/themes/imbalance/header.php',
				cleanFile:'/root/sites/closingimbalance_com/wp-content/themes/imbalance/header.php'
			}
		],
		clam: {
			exempt : [
				"/ninjafirewall/lib/share/sigs.txt: SecuriteInfo.com.JS.Exploit-16.UNOFFICIAL FOUND"
			]
		},
		noPHP : [
			{ 
				path: "wp-content/uploads",
				action: "delete",
				exempt: []
			}
		],
		clean : [
			{
				compareRoot: "/var/www/clean/wordpress/",
				path: "./",
				action: "replace",
				recurse: false,
				exempt: []
			},
			{
				compareRoot: "/var/www/clean/wordpress/",
				path: "wp-admin/",
				action: "replace",
				recurse: true,
				exempt: []
			},
			{
				compareRoot: "/var/www/clean/wordpress/",
				path: "wp-includes/",
				action: "replace",
				recurse: true,
				exempt: []
			}
		]		
	},
	{
		site:"fortunesrocks.me",
		type:"wordpress",
		database: "fme",
		databaseUser: "fme",
		databasePassword:"g2g1793",
		root:"/var/www/fortunesrocks_me/wordpress",
		contacts:"watch@fortunesrocks.me",
		ownerEmail:"fme@fortunesrocks.me",
		ownerName:"Marlin Cobb",
		watchFiles : [
		],
		clam: {
			exempt : [
				"/ninjafirewall/lib/share/sigs.txt: SecuriteInfo.com.JS.Exploit-16.UNOFFICIAL FOUND"
			]
		},
		noPHP : [
			{ 
				path: "wp-content/uploads",
				action: "delete",
				exempt: []
			}
		],
		clean : [
			{
				compareRoot: "/var/www/clean/wordpress/",
				path: "./",
				action: "replace",
				recurse: false,
				exempt: []
			},
			{
				compareRoot: "/var/www/clean/wordpress/",
				path: "wp-admin/",
				action: "replace",
				recurse: true,
				exempt: []
			},
			{
				compareRoot: "/var/www/clean/wordpress/",
				path: "wp-includes/",
				action: "replace",
				recurse: true,
				exempt: []
			}
		]		
	},
	{
		site:"fortunesrocks.org",
		type:"wordpress",
		database: "frocks_org",
		databaseUser: "frocks_org",
		databasePassword:"g2g1793",
		root:"/var/www/fortunesrocks_org/wordpress",
		contacts:"watch@fortunesrocks.me",
		ownerEmail:"forg@fortunesrocks.me",
		ownerName:"Marlin Cobb",
		watchFiles : [
		],
		clam: {
			exempt : [
				"/ninjafirewall/lib/share/sigs.txt: SecuriteInfo.com.JS.Exploit-16.UNOFFICIAL FOUND"
			]
		},
		noPHP : [
			{ 
				path: "wp-content/uploads",
				action: "delete",
				exempt: []
			}
		],
		clean : [
			{
				compareRoot: "/var/www/clean/wordpress/",
				path: "./",
				action: "replace",
				recurse: false,
				exempt: []
			},
			{
				compareRoot: "/var/www/clean/wordpress/",
				path: "wp-admin/",
				action: "replace",
				recurse: true,
				exempt: []
			},
			{
				compareRoot: "/var/www/clean/wordpress/",
				path: "wp-includes/",
				action: "replace",
				recurse: true,
				exempt: []
			}
		]		
	},
	{
		site:"marksupholstering.com",
		type:"wordpress",
		database: "marks",
		databaseUser: "marks",
		databasePassword:"g2g1793",
		root:"/var/www/marksupholstering_com/wordpress",
		contacts:"watch@fortunesrocks.me",
		ownerEmail:"marksupholstering@fortunesrocks.me",
		ownerName:"Marlin Cobb",
		watchFiles : [
		],
		clam: {
			exempt : [
				"/ninjafirewall/lib/share/sigs.txt: SecuriteInfo.com.JS.Exploit-16.UNOFFICIAL FOUND"
			]
		},
		noPHP : [
			{ 
				path: "wp-content/uploads",
				action: "delete",
				exempt: []
			}
		],
		clean : [
			{
				compareRoot: "/var/www/clean/wordpress/",
				path: "./",
				action: "replace",
				recurse: false,
				exempt: []
			},
			{
				compareRoot: "/var/www/clean/wordpress/",
				path: "wp-admin/",
				action: "replace",
				recurse: true,
				exempt: []
			},
			{
				compareRoot: "/var/www/clean/wordpress/",
				path: "wp-includes/",
				action: "replace",
				recurse: true,
				exempt: []
			}
		]		
	},
	{
		site:"naswcanews.org",
		type:"wordpress",
		database: "naswcanews_org",
		databaseUser: "naswcanews",
		databasePassword:"g2g1793",
		root:"/var/www/naswcanews_org/wordpress",
		contacts:"watch@fortunesrocks.me",
		ownerEmail:"marksupholstering@fortunesrocks.me",
		ownerName:"Lisa",
		watchFiles : [
		],
		clam: {
			exempt : [
				"/ninjafirewall/lib/share/sigs.txt: SecuriteInfo.com.JS.Exploit-16.UNOFFICIAL FOUND"
			]
		},
		noPHP : [
			{ 
				path: "wp-content/uploads",
				action: "delete",
				exempt: []
			}
		],
		clean : [
			{
				compareRoot: "/var/www/clean/wordpress/",
				path: "./",
				action: "replace",
				recurse: false,
				exempt: []
			},
			{
				compareRoot: "/var/www/clean/wordpress/",
				path: "wp-admin/",
				action: "replace",
				recurse: true,
				exempt: []
			},
			{
				compareRoot: "/var/www/clean/wordpress/",
				path: "wp-includes/",
				action: "replace",
				recurse: true,
				exempt: []
			}
		]		
	},
	{
		site:"oripsllc.com",
		type:"wordpress",
		database: "oripsllc_com",
		databaseUser: "oripsllc",
		databasePassword:"g2g1793",
		root:"/var/www/oripsllc_com/wordpress",
		contacts:"watch@fortunesrocks.me",
		ownerEmail:"oripsllc@fortunesrocks.me",
		ownerName:"Marlin Cobb",
		watchFiles : [
		],
		clam: {
			exempt : [
				"/ninjafirewall/lib/share/sigs.txt: SecuriteInfo.com.JS.Exploit-16.UNOFFICIAL FOUND"
			]
		},
		noPHP : [
			{ 
				path: "wp-content/uploads",
				action: "delete",
				exempt: []
			}
		],
		clean : [
			{
				compareRoot: "/var/www/clean/wordpress/",
				path: "./",
				action: "replace",
				recurse: false,
				exempt: []
			},
			{
				compareRoot: "/var/www/clean/wordpress/",
				path: "wp-admin/",
				action: "replace",
				recurse: true,
				exempt: []
			},
			{
				compareRoot: "/var/www/clean/wordpress/",
				path: "wp-includes/",
				action: "replace",
				recurse: true,
				exempt: []
			}
		]		
	},
	{
		site:"redliontrader.com",
		type:"wordpress",
		database: "redliontrader_com",
		databaseUser: "root",
		databasePassword:"tulipbulb",
		root:"/var/www/redliontrader_com/wordpress",
		contacts:"watch@fortunesrocks.me",
		ownerEmail:"marksupholstering@fortunesrocks.me",
		ownerName:"Marlin Cobb",
		watchFiles : [
		],
		noPHP : [
			{ 
				path: "wp-content/uploads",
				action: "delete",
				exempt: []
			}
		],
		clam: {
			exempt : [
				"/ninjafirewall/lib/share/sigs.txt: SecuriteInfo.com.JS.Exploit-16.UNOFFICIAL FOUND"
			]
		},
		clean : [
			{
				compareRoot: "/var/www/clean/wordpress/",
				path: "./",
				action: "replace",
				recurse: false,
				exempt: []
			},
			{
				compareRoot: "/var/www/clean/wordpress/",
				path: "wp-admin/",
				action: "replace",
				recurse: true,
				exempt: []
			},
			{
				compareRoot: "/var/www/clean/wordpress/",
				path: "wp-includes/",
				action: "replace",
				recurse: true,
				exempt: []
			}
		]		
	},
	{
		site:"ttthedge.com",
		type:"wordpress",
		database: "U1732607",
		databaseUser: "ttthedge_com",
		databasePassword:"g2g1793",
		root:"/var/www/ttthedge_com/wordpress",
		contacts:"watch@fortunesrocks.me",
		ownerEmail:"marksupholstering@fortunesrocks.me",
		ownerName:"Marlin Cobb",
		watchFiles : [
		],
		clam: {
			exempt : [
				"/ninjafirewall/lib/share/sigs.txt: SecuriteInfo.com.JS.Exploit-16.UNOFFICIAL FOUND"
			]
		},
		noPHP : [
			{ 
				path: "wp-content/uploads",
				action: "delete",
				exempt: []
			}
		],
		clean : [
			{
				compareRoot: "/var/www/clean/wordpress/",
				path: "./",
				action: "replace",
				recurse: false,
				exempt: []
			},
			{
				compareRoot: "/var/www/clean/wordpress/",
				path: "wp-admin/",
				action: "replace",
				recurse: true,
				exempt: []
			},
			{
				compareRoot: "/var/www/clean/wordpress/",
				path: "wp-includes/",
				action: "replace",
				recurse: true,
				exempt: []
			}
		]		
	},
	{
		site:"wpress4.me",
		type:"wordpress",
		database: "wpress4_me",
		databaseUser: "wpress4_me",
		databasePassword:"g2g1793",
		root:"/var/www/wpress4_me/wordpress",
		contacts:"watch@fortunesrocks.me",
		ownerEmail:"wpress4me@fortunesrocks.me",
		ownerName:"Marlin Cobb",
		watchFiles : [
		],
		clam: {
			exempt : [
				"/ninjafirewall/lib/share/sigs.txt: SecuriteInfo.com.JS.Exploit-16.UNOFFICIAL FOUND"
			]
		},
		noPHP : [
			{ 
				path: "wp-content/uploads",
				action: "delete",
				exempt: []
			}
		],
		clean : [
			{
				compareRoot: "/var/www/clean/wordpress/",
				path: "./",
				action: "replace",
				recurse: false,
				exempt: []
			},
			{
				compareRoot: "/var/www/clean/wordpress/",
				path: "wp-admin/",
				action: "replace",
				recurse: true,
				exempt: []
			},
			{
				compareRoot: "/var/www/clean/wordpress/",
				path: "wp-includes/",
				action: "replace",
				recurse: true,
				exempt: []
			}
		]		
	},
	
]