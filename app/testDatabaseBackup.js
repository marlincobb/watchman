"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
let log4js = require('log4js');
log4js.replaceConsole();
let L = log4js.getLogger();
const databaseBackup_1 = require("./databaseBackup");
var db = new databaseBackup_1.databaseBackup();
L.info("Getting ready to call backup all");
db.dumpAllDbs();
L.info("DONE");
//# sourceMappingURL=testDatabaseBackup.js.map