//
// FortunesRocks.me special Wordpress Antivirus scanner
//
	var walk = require("fs-walk");
	const execSync = require("child_process").execSync;
	const exec = require("child_process").exec;

var  log4js = require('log4js');
log4js.replaceConsole();
var L = log4js.getLogger();

var fs = require('fs')

export class fmeAV {
	version:string = "1.0";
	debug:boolean = false;

	constructor() {
		L.info ("fmeAV loaded - Version:",this.version);
	}
 
	scanGlobals = (dir)  => {
		var debug = this.debug;
		return new Promise< any >( (resolve,reject) => {
			
			var countGlobals = (file)  => {
				var count = parseInt(execSync('grep "GLOBALS"  -o "'+file+'" | wc -l'),10);
				if (!count) {
					L.error("Looking a file",file,"returned NAN for global count");
					return 0
				}
				return count
			}
			
			var currentBase = dir;
			var errors = [];
			walk.files(dir,function(basedir,filename,stat,next) {
				if (currentBase != basedir) {
					L.info("checking directory",basedir)
					currentBase = basedir;
				}
				if (debug) L.info("scanGlobals: checking file",basedir,filename);
				filename = filename.replace(/" "/g,"\ ");
				var count = countGlobals(basedir+"/"+filename);
				if (count > 0 ) {
					var fSize = stat.size;
					var gDensity = count / fSize;
					if (count > 100 && (gDensity * 100) > 1) {
						L.error("Too Many Globals!",count,basedir + "/" + filename,(gDensity*100).toFixed(2) );
						errors.push("count: "+ count + " file: "+ basedir + "/" + filename +" density: " + (gDensity*100).toFixed(2))
					}
				}
				next();
			},function(err) {
				if (err) {
					reject(err);
				} else {
					L.info("fmeAV:scanGlobals: completed",dir);
					resolve(errors);
				}
			})
		})	
	}
	
	scanClean = (dir) => {
		return new Promise< any > ((resolve,reject) => {
			var buffer = "";
			L.info("fmeAV:scanClean",dir);
			var cmd = "diff -qr /var/www/clean/wordpress/ " + dir; 
			var scan = exec(cmd);

			scan.stdout.on("data",function(data) {
				// L.info("fmeAV.scanClean.stdout:",data)
				buffer += data;
			});

			scan.stderr.on("data",function(data) {
				L.info("fmeAV.scanClean.stderr:",data)
				reject(data)
			});

			scan.on("close",function(code) {
				L.info("fmeAV.scanClean.close:",code);
				if (buffer.length > 0) {
					var lines = buffer.split("\n");
					L.info("resolving lines:",lines.length);
					resolve(lines);
				} else {
					L.info("fmeAv.scanClean, complete, callling callback");
					resolve([])
				}
			});
		})
	}
	
	
	
}