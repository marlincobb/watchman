import {website} from "./websites";
var fs = require('fs');
var execSync = require('child_process').execSync;
let  log4js = require('log4js');
log4js.replaceConsole();
let L = log4js.getLogger();
var AdmZip = require('adm-zip');
var _m = require("moment-timezone");

export class databaseBackup {
	year = _m().year().toString();
	dumpSQL = (dbName:string, dbUser:string, dbPwd:string, savePath:string) => {
		var cmd:string = "mysqldump -u ";
		cmd += dbUser + " ";
		cmd += "-p"+ dbPwd + " --max_allowed_packet=512M ";
		cmd += dbName + " > ";
		cmd += savePath + "/../backup.sql";
		// L.debug("CMD =>", cmd)
		var status = execSync (cmd);
		status = execSync("zip " + savePath+"/../backup.sql.zip " + savePath+"/../backup.sql");
		fs.unlinkSync(savePath+"/../backup.sql");
		return status.toString();
	}
	
	s3Backup = (dir:string, bucket:string) => {
		var cmd = "/root/s3cmd/s3cmd sync --recursive --preserve  --no-check-md5 --limit-rate=15M "+ dir +" "+ bucket;
		L.info("databaseBackup => saving",cmd);
		return execSync(cmd);
	}
	
	//
	// ==> note.. this is a sync process
	//
	dumpAllDbs = () => {
		var WWW = new website();
		var weblist = WWW.getListFromFile();
		for (var i = 0; i < weblist.length; i++) {
			var site = weblist[i];
			L.info("databaseBackup:=> Started for ",site.site);
			if (weblist[i].type.toLowerCase() === "wordpress") {
				var savePath = site.root;
			// dump SQL =>	
				var status = this.dumpSQL ( site.database, site.databaseUser, site.databasePassword, savePath);
			// copy wp-content =>	
				L.info("databaseBackup: sending to s3",site.root+"/wp-content/","to: s3://"+this.year+"-web6/"+site.site+"/");
				status = this.s3Backup( site.root+"/wp-content/", "s3://"+this.year+"-web6/"+site.site+"/wp-content/");
			// copy dumped SQL to s3	
				L.info("databaseBackup: sending to s3",site.root+"/../backup.sql.zip","to => s3://"+this.year+"-web6/"+site.site+"/config/");
				status = this.s3Backup(site.root+"/../backup.sql.zip","s3://"+this.year+"-web6/"+site.site+"/config/");
			}
		}
		var status = this.s3Backup("/etc","s3://"+this.year+"-web6/config/etc/");
		status = this.s3Backup("/root","s3://"+this.year+"-web6/config/root/");
		L.info("databaseBackup:dumpAllDbs finished");
		return;
	}
	
	
}