"use strict";
//
//
// Get info about websites
//
Object.defineProperty(exports, "__esModule", { value: true });
let fs = require("fs");
let log4js = require('log4js');
log4js.replaceConsole();
let L = log4js.getLogger();
class website {
    constructor() {
        this.getListFromDir = () => {
            return new Promise((resolve, reject) => {
                var rawList = fs.readdirSync(this.hostRoot);
                if (typeof rawList != "object") {
                    L.error("no directories found at root", this.hostRoot);
                    resolve(null);
                }
                var list = [];
                for (var i in rawList) {
                    if (rawList[i].indexOf("_") > -1)
                        list.push(rawList[i]);
                }
                if (list.length) {
                    resolve(list);
                    //		L.debug("Websites to monitor are",list);
                }
                else {
                    L.error("no websites found in this directory!");
                    resolve(null);
                }
            });
        };
        this.getListFromFile = () => {
            L.info("websites: Load list from file", this.filePath);
            var ws = fs.readFileSync(this.filePath);
            return JSON.parse(ws.toString());
        };
        this.hostRoot = "/var/www/";
        this.siteRoot = "/wordpress/";
        this.filePath = "/home/nodeapps/watchman/websites.json";
    }
}
exports.website = website;
//# sourceMappingURL=websites.js.map