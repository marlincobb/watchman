module.exports = function () {
//
// ===> Site Compares
//
	fs = require("fs");
	const exec = require('child_process').exec;
	const execSync = require('child_process').execSync;
	var walk = require("fs-walk");
	filter = {};
	filter.version = "1.0";
	filter.log = "";
	filter.exceptions = [];
	filter.goodFullPath = {};
	filter.badFullPath = {};
	filter.jailPath = {};
	filter.paths = [
		{path : "wordpress", false : "true"},
		{path : "wordpress/wp-admin", recurse : "true"},
		{path : "wordpress/wp-content", recurse : "false"},
		{path : "wordpress/wp-content/themes/twentyfifteen", recurse : "false"},
		{path : "wordpress/wp-content/themes/twentyfourteen", recurse : "false"},
		{path : "wordpress/wp-content/themes/twentysixteen", recurse : "false"},
		{path : "wordpress/wp-includes", recurse : "true"},
	]
	filter.init = function () {
		L.info("filter:diffs - version",filter.version);
	}
	filter.scanWordPressSite = function(website,cb) {
		function exempt (error) {
			var exempts = website.clean.exempt;
			if (exempts.length > 0) {
				for (var idx in exempts) {
					if (error == exempts[idx]) return true
				}
			}
			return false;
		}
		//var resp = execSync("diff -qr /var/www/clean/wordpress "+website.root);
		var resp = execSync('diff -q -r /var/www/clean/wordpress '+website.root+' | cat'); //' | wc -l'),10);
		// L.info("scanWordpress",resp.toString())
		var list = resp.toString().split("\n");
		list.pop(); // remove that last item
		var errors = [];
		for (var idx in list) {
			var error = list[idx];
			if (!exempt(error)) errors.push(error);
		}
		if (typeof cb == "function") cb(errors)
		return(errors)
	}
	
	filter.compareCleanToWebsite = function (dir) {
		var count = parseInt(execSync('diff -qr /var/www/clean/wordpress '+dir+'/wordpress | wc -l'),10);
	   L.info("Website:",dir,"Has a diff-count of:",count);
	}
	
	
	
	filter.jail = function () {
		execSync('mv '+ badFullPath+" "+filter.jailPath);
		L.error()
	}
	
	filter.delete = function () {
		execSync('rm '+ badFullPath);
		L.error("filter:diff deleted the file:",badFullPath);
		
	}
	filter.replace = function () {
		execSync('cp '+ goodFullPath + " " + badFullPath);
		L.error("filter:diff Replaced the file:",badFullPath,"with file:",goodFullPath);
		
	}
	
	
	return filter
}