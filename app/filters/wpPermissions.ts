
	var log4js = require('log4js');
	log4js.replaceConsole();
	let L = log4js.getLogger();

	var fs = require("fs");
	var walk = require("fs-walk");
	var path = require ("path");
	var userid = require("userid");

	const execSync = require("child_process").execSync;
	const exec = require("child_process").exec;

export class wpPermissions {
	version:string = "1.0";
	debug:boolean = false;
	filePermission:string = "644" ;
	directoryPermission:string = "755" ;

	constructor () {
		L.info("wpPermissions contructed.")
	}
	
	checkUser = (user) => {
		return new Promise ( (resolve, reject ) => {
			var test = false;
			var r = execSync("getent passwd "+user).toString();
			try {
				if (this.debug) L.info("wpPermissions:checkUser returned:",r);
			} catch(err) {
				L.info("Error returned from checking user",err);
				r=null;
			}
			if (r.length > 1) test = true;
			resolve(test);
		})	
	}
	
	setPermissions  = (website) => {
		return new Promise ( (resolve,reject) => {
			if (this.debug) L.info("wpPermissions.setPermissions for website",website.owner,website.site);
			walk.walk(website.root,function(basedir, filename, stat, next ) {
				var perm = stat.isDirectory() ? this.directoryPermissions : this.filePermissions;
				fs.chmodSync( path.join(basedir, filename), perm, next);
			},function(err) {
				if (err) {
					 L.error("wPermissions.permissions:",err);
					reject (err);
				} else {
					resolve(true);
				}
			})
		 })	
	}
	
	setOwner  = (website,cb) => {
		return new Promise ( (resolve,reject) => {
			if (this.debug) L.info("wpPermissions.setOwer/Group for website",website.owner,website.site);
			walk.walk(website.root,function(basedir, filename, stat, next ) {
				var perm = stat.isDirectory() ? this.directoryPermissions : this.filePermissions;
				var uid = userid.uid(website.user);
				var gid = userid.gid(website.user);
				fs.chownSync( path.join(basedir, filename), uid, gid, next);
			},function(err) {
				if (err) {
					L.error("wPermissions.setOwner:",err);
					reject(err)
				} else {
					resolve(true);
				}
			})
		});
	}
	
	checkOwner = (website) => {
		return new Promise ( (resolve, reject ) => {
			var fcount = 0;
			var errors = [];
			var uid = userid.uid(website.user);
			var gid = userid.gid(website.user);
			walk.walk(website.root,function( basedir , filename, stat, next) {
				if (stat.gid != gid) errors.push("File: "+ filename +" expected groupid "+ gid +" instead has gid "+ stat.gid + " " + userid.groupname(stat.gid) )
				if (stat.uid != uid) errors.push("File: "+ filename +" expected userid "+ uid + ""+ userid.username(uid) + " instead has uid "+stat.uid +" "+ userid.username(stat.uid) )
				if (errors.length > 100) {
					L.error("wpPermissions.checkowner error overflow",errors.length)
					errors.push("Errors terminated cause they are over 100!");
					resolve (errors);
					return
				}
				fcount++;
				next();
			},function(err) {
				if (err)  {
					L.error("wPermissions.setOwner:",err);
					reject (err);
				} else {
					resolve (errors);
				}
			})
		})	
	}
	
	checkPermissions = (website) => {
			var directoryPermission = this.directoryPermission;
			var filePermission = this.filePermission;
		
			function exempt(filename) {
				for (var idx in website.wpPermissions.exempt) {
					if (filename == website.wpPermissions.exempt[idx]) return true;
				}
				return false;
			}
		
		return new Promise ( (resolve,reject) => {
			var fcount = 0; 
			var errors = [];
			walk.walk(website.root,function (basedir, filename, stat,next) {
				var perm = (stat.mode & 0o777).toString(8);
		//		if (fcount == 0 ) L.info("FileStat is",stat,"Perm is",perm)
				// var perm = "644";//(stat.mode && "0777").toString(8);
			//	L.info("state.Mode",stat.mode)
				if ( stat.isDirectory() ) {
					if (perm != directoryPermission) {
						if ( !exempt(filename) ) errors.push("wpPermissions.checkPermissions expected:"+directoryPermission+" found: "+perm+" file "+filename+" basedir "+basedir)
					}
				} else {
					if (perm != filePermission) {
						if ( !exempt(filename) ) errors.push("wpPermissions.checkPermissions expected: "+filePermission+" found: "+perm+" file "+filename+" basedir "+basedir)
					}
				}
				if (errors.length > 100) {
					errors.push("wpPermissions.checkPermissions terminated, error overflow"+website.root);
					resolve(errors);
					return
				}
				fcount++;
				next();
			},function(err){
				if (err) {
					L.error("wpPermissions.checkPermissions returned error",err);
					reject (err);
				} else {
					resolve (errors)
				}
			});
		});
	}
	
}