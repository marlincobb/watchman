"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var path = require("path");
var fs = require('fs');
const execSync = require('child_process').execSync;
var log4js = require('log4js');
log4js.replaceConsole();
var L = log4js.getLogger();
class surveillance {
    constructor() {
        this.version = "1.0";
        this.options = {};
        this.cleanRoot = "/root/cleanroom";
        this.debug = false;
        this.watch = function (file, clean, website) {
            var debug = this.debug;
            return new Promise((resolve, reject) => {
                if (debug)
                    L.info("watchman:surveillance: started for", file);
                var w = fs.watch(file, function (e, fn) {
                    w.close(function () {
                        L.error("########## RETURNED FROM w.close! ############");
                    });
                    if (debug)
                        L.info("watchman:surveillance: detected change", e, fn);
                    if (typeof cb == "function")
                        cb(file, clean, website);
                });
            });
        };
        this.clean = function (file, clean, website, cb) {
            if (surveillance.debug)
                L.info("watchman:surveillance: clean copy restored from ", clean);
            execSync('cp ' + clean + ' ' + file);
            if (typeof cb == "function")
                cb(file, clean, website);
        };
    }
}
exports.surveillance = surveillance;
//# sourceMappingURL=surveillance.js.map