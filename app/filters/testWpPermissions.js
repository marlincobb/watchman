//
// Set up logging
//
var log4js = require('log4js');
log4js.replaceConsole();
L = log4js.getLogger();
//
// get EmailAlets
//
var email = require("/home/nodeapps/watchman/app/emailAlert.js")()
email.init();
var wpp = require("./wpPermissions.js")();
L.info("Starting test of wpPermissions: version:",wpp.version);
var user = "mrtopstep_com";
L.info("testing for user",user);
var r = wpp.checkUser(user,function(v) {
	L.info("checkUser callback returned",v);
});
L.info("wpp.checkuser returned",r);
var user = "redliontrader_com"
L.info("testing for user",user);
var r = wpp.checkUser(user,function(v) {
	L.info("checkUser callback returned",v);
});
L.info("wpp.checkuser returned",r);

var fs = require("fs");
var mode = fs.statSync("/home/nodeapps/watchman/watchman.js").mode & 0777;
L.info("file: ",mode.toString(8))

//var website = require ("/home/nodeapps/watchman/wpress4_me.json");
// L.info("testing with website",website);
var websites = require ("/home/nodeapps/watchman/websites.json");
var wcount = 0

check(websites,wcount);
function check(websites,wcount) {	
	L.info("checking website",websites[wcount].site,wcount);
	var website = websites[wcount];
	wpPermissions.checkOwner(website,function(errors) {
		//L.info("wpPermissions returned",errors)
		L.info (website.site,"returned the following");
		for (var i in errors) {
			L.error(errors[wcount]);
		}
		wcount++;
		L.info("i",wcount,"websites.length",websites.length);
		if (wcount < websites.length-1) {
			check(websites,wcount);
			return
		}	
		L.info("******** complete **********");
		wcount = 0;
		checkP(websites,wcount);
	})
}

function checkP(websites,wcount) {	
	L.info("checking permissions",websites[wcount].site,wcount);
	var website = websites[wcount];
	wpPermissions.checkPermissions(website,function(errors) {
		//L.info("wpPermissions returned",errors)
		L.info (website.site,"returned the following");
		if (errors.length > 0)
			email.template("watchmanErrors.jade","CheckPermissions Error: "+website.site,"wpPermissions.checkPermissions",website,errors)
		for (var index in errors) {
			L.error(errors[index]);
		}
		wcount++;
		if (wcount < websites.length) {
			checkP(websites,wcount);
			return
		}
		L.info("**************** wpPermissions:checkP has completed ***************");
		return;
	})
}