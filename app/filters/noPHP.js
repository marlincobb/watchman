/*
	Filename: noPHP.js
	Purpose: Filter for FortunesRocks.me LLC watchman.  Runs through a list of 
	directories that should NOT have any PHP files in it.
	
	Needs to have a logger passed in that support the following methods
	(L. info, debug, error, warn)
	


*/
module.exports = function() {
	fs = require("fs");
	const exec = require('child_process').exec;
	const execSync = require('child_process').execSync;
	const path = require("path");
	filter = {};
	filter.version = "1.0";
	filter.paths  = [
		{path:"/wordpress/wp-content/uploads", recursive:true }
	]
	
	filter.scanDirectoryRecursive = function (dir,cb) {
		errors = execSync('find '+dir+' -type f -name "*.php"').toString().split("\n");
		errors.pop();
		errors.push( execSync('find '+dir+' -type f -name "*.js"').toString().split("\n"))
		errors.pop();
		if (errors.length == 0 ) L.info("no files found!");
		L.info("Result from scanDirRecursive is:",errors.length,errors[0]);
		if (typeof cb == "function") cb(errors);
		return errors;
	}
	filter.scanDirectory = function (dir) {
		results = execSync('find '+dir+' -type f -maxdepth 1 -name "*.php"').toString().split("\n")
		L.info("Result from scanDirectory is:",results.length,results[0]);
		return results;
	}
	filter.moveFile = function(filePath) {
		var file = path.basename(filePath);
		results = execSync("mv "+file+" /root/bad/"+file);
	}
	filter.removeFile = function(filePath) {
		var file = path.basename(filepath);
		result = execSync("rm "+file);
		return result;
	}
	return filter;
	
	
}