"use strict";
//
//
// Control Clamscan
//
Object.defineProperty(exports, "__esModule", { value: true });
var log4js = require('log4js');
log4js.replaceConsole();
var L = log4js.getLogger();
var fs = require('fs');
const exec = require('child_process').exec;
const execFile = require('child_process').execFile;
class clamScan {
    constructor() {
        this.version = "1.0";
        //	clamscan.settings.debug_mode = true;
        this.command = "clamscan --infected --recursive --no-summary ";
        this.commandFile = "clamscan";
        this.argumentsFile = ["--infected", "--recursive", "--no-summary"];
        this.settings = { debug: true };
        this.clamscan = require('clamscan')({
            clamdscan: {
                "config-file": "/etc/clamav/clamd.conf"
            },
            preference: 'clamscan',
            debug_mode: true,
            recursive: true
        });
        this.scanDirectory = (path) => {
            return new Promise((resolve, reject) => {
                var foundVirus = false;
                if (this.settings.debug)
                    L.info("clam.scanDirectory: Started", path);
                exec(this.command + path + " | cat", function (err, stdOut, stdErr) {
                    L.info("clam.scanDirectory Finished:", err, stdOut, stdErr);
                    var files = stdOut.trim().split("\n");
                    if (files[0] !== '') {
                        foundVirus = true;
                        L.info("clam:scanDirectory found virus", files);
                    }
                    resolve({ foundVirus, files });
                });
            });
        };
        this.refresh = function () {
            return new Promise((resolve, reject) => {
                L.info("clam.refresh: freshing up the clam database");
                exec('freshclam --quiet', { maxBuffer: 2024 * 1024 }, function (err, stdOut, stdErr) {
                    if (err) {
                        L.error("clam.refresh: error during freshening up the clam databases", err, "stdErr", stdErr);
                        if (stdErr.indexOf("Problem with internal logger") > -1) {
                            L.error("clam: freshclam log is bad, delete and restart");
                            fs.unlinkSync("/var/log/clamav/freshclam.log");
                            this.refresh();
                        }
                        return;
                    }
                    L.info("clam.refresh completed", stdOut);
                    resolve(true);
                });
            });
        };
        L.info("clam module loaded", this.version);
        this.exempt = [];
    }
}
exports.clamScan = clamScan;
//# sourceMappingURL=clamscan.js.map