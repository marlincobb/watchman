"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const websites_1 = require("./websites");
var fs = require('fs');
var execSync = require('child_process').execSync;
let log4js = require('log4js');
log4js.replaceConsole();
let L = log4js.getLogger();
var AdmZip = require('adm-zip');
var _m = require("moment-timezone");
class databaseBackup {
    constructor() {
        this.year = _m().year().toString();
        this.dumpSQL = (dbName, dbUser, dbPwd, savePath) => {
            var cmd = "mysqldump -u ";
            cmd += dbUser + " ";
            cmd += "-p" + dbPwd + " --max_allowed_packet=512M ";
            cmd += dbName + " > ";
            cmd += savePath + "/../backup.sql";
            // L.debug("CMD =>", cmd)
            var status = execSync(cmd);
            status = execSync("zip " + savePath + "/../backup.sql.zip " + savePath + "/../backup.sql");
            fs.unlinkSync(savePath + "/../backup.sql");
            return status.toString();
        };
        this.s3Backup = (dir, bucket) => {
            var cmd = "/root/s3cmd/s3cmd sync --recursive --preserve  --no-check-md5 --limit-rate=15M " + dir + " " + bucket;
            L.info("databaseBackup => saving", cmd);
            return execSync(cmd);
        };
        //
        // ==> note.. this is a sync process
        //
        this.dumpAllDbs = () => {
            var WWW = new websites_1.website();
            var weblist = WWW.getListFromFile();
            for (var i = 0; i < weblist.length; i++) {
                var site = weblist[i];
                L.info("databaseBackup:=> Started for ", site.site);
                if (weblist[i].type.toLowerCase() === "wordpress") {
                    var savePath = site.root;
                    // dump SQL =>	
                    var status = this.dumpSQL(site.database, site.databaseUser, site.databasePassword, savePath);
                    // copy wp-content =>	
                    L.info("databaseBackup: sending to s3", site.root + "/wp-content/", "to: s3://" + this.year + "-web6/" + site.site + "/");
                    status = this.s3Backup(site.root + "/wp-content/", "s3://" + this.year + "-web6/" + site.site + "/wp-content/");
                    // copy dumped SQL to s3	
                    L.info("databaseBackup: sending to s3", site.root + "/../backup.sql.zip", "to => s3://" + this.year + "-web6/" + site.site + "/config/");
                    status = this.s3Backup(site.root + "/../backup.sql.zip", "s3://" + this.year + "-web6/" + site.site + "/config/");
                }
            }
            var status = this.s3Backup("/etc", "s3://" + this.year + "-web6/config/etc/");
            status = this.s3Backup("/root", "s3://" + this.year + "-web6/config/root/");
            L.info("databaseBackup:dumpAllDbs finished");
            return;
        };
    }
}
exports.databaseBackup = databaseBackup;
//# sourceMappingURL=databaseBackup.js.map