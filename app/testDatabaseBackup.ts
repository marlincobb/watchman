let  log4js = require('log4js');
log4js.replaceConsole();
let L = log4js.getLogger();

import {databaseBackup} from "./databaseBackup";

var db = new databaseBackup();
L.info("Getting ready to call backup all");

db.dumpAllDbs();

L.info("DONE");
