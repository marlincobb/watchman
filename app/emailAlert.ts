
var  log4js = require('log4js');
log4js.replaceConsole();
var L = log4js.getLogger();
var jade = require('pug');
var nodemailer = require('nodemailer');
var sendmailTransport = require('nodemailer-sendmail-transport');

export class emailAlert {
	debug:boolean = false;
	options = {
		from : "",
		to: "", 
		subject: "",
		text: "",
		html: "",
	};
	mail:any;
	version = "1.0";
	
	constructor() {
		L.info("Email Alerts Version: ", this.version);
		if (this.debug) L.info("emailAlerts:init");
		this.mail = nodemailer.createTransport(sendmailTransport(this.options));
		this.options.from = "watchmanAlert<emailAlert@fortunesrocks.me>";
		this.options.to = "emailAlert@fortunesrocks.me";
	}
	
	send = (subject,message) => {
		return new Promise ( (resolve,reject) => {
			if (this.debug) L.info("emailAlerts:send");
			var options = this.options;
			options.subject = subject;
			options.text = "none";
			options.html = message;
			this.mail.sendMail(options,function(err,info) {
				if (err) {
					L.error("emailAlerts:send.err:",err,info);
					reject(err)
				} else {
					if (this.debug) L.info("emailAlerts:send => Email sent");
					resolve(true);
				}
			})
		});	
	}
	
	template = (template,subj,filter,website,errors) =>  {
		var debug = this.debug;
		var options = this.options;
		return new Promise ( (resolve,reject) => { 
			if (debug) L.info("emailAlerts:template called",template);
			options.html = jade.renderFile("/home/nodeapps/watchman/emails/"+template,{filterName:filter,website:website,errors:errors})
			options.subject = subj;
			if (debug) L.info("emailAlerts: sending email:",options);
			this.mail.sendMail(options,function(err,info) {
				if (err) {
					L.error("emailAlerts:template:",err,info);
				 	reject(err);
				} else {
					resolve (info);
				}
			})
		});	
	}
	
}