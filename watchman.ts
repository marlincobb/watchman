//
// --> Watchman is a product of Fortunesrocks.me LLC
// --> Purpose : Administor saftey / compliance / security for FortunesRocks.me servers
//
//
var log4js = require('log4js');
log4js.replaceConsole();
let L = log4js.getLogger();
let version = "2.00";

L.info("===================================================");
L.info("====>> Watchman Version:",version," Started <<========");
L.info("===================================================");

var fs = require('fs')
var mongoose = require('mongoose')
import * as _m from "moment-timezone"; 
import {website} from "./app/websites";
import {emailAlert} from "./app/emailAlert";
import {wpPermissions} from "./app/filters/wpPermissions";
import {fmeAV} from "./app/fmeAV";
let fme = new fmeAV();
var wpPermission = new wpPermissions();

let email = new emailAlert();

let w = new website(); 
import {clamScan} from "./app/clamscan";
import {databaseBackup} from "./app/databaseBackup";

let clam = new clamScan();

var websites = require ("./websites.json");
clam.exempt.push("/ninjafirewall/lib/share/sigs.txt: SecuriteInfo.com.JS.Exploit-16.UNOFFICIAL FOUND");
let currentDay
var poll = () => {
	var dbBackup = new databaseBackup();
	dbBackup.dumpAllDbs();
	var currentDate = _m().format("YYYYMMDD");
	setInterval( () => {
		if (_m().format("YYYYMMDD") != currentDate) {
			L.info("watchman.dbBackup time");
			currentDate = _m().format("YYYYMMDD");

			dbBackup.dumpAllDbs();
		}
	},1000*60*60);
}

function go () {
	clam.refresh().then( async (status) => {
	//	let go = async () => { 
			L.info("beginning to scan sites: ");
			var list:any = [];
			list = await w.getListFromDir();
			if (!list) { L.error("Error getting the directory list");return }
			L.info( "watchman: sweeping the following sites:",JSON.stringify(list) )

		// ==> Loop through sites scanning.	

			for (var i=0; i < websites.length; i++) {
				var W = websites[i];
				var sitePath = W.root;
				L.info("*** SCANNING SITE",sitePath);

			// ==> Clam AV scan

				var results:any = await clam.scanDirectory(sitePath);
				results = removeExempts(results,W,"clam");
				if (results.length)  email.template("clam","Watchman Alert => Clam Scan:"+W.site,"clamAV",W,results);

			// ==>  Clean Scan 	

				var results = await fme.scanClean(sitePath);
				L.info("ScanClean returned",results.length);
				results = removeExempts(results,W,"clean");
				//L.info("results now are",results);
				if (results.length) var emailStatus = await email.template("watchmanErrors.jade","Watchman Alert => Clean Scan:"+W.site,"Clean",W,results);	

			// ==> Check Permissions

				L.info("Starting check permissions")
				results = await wpPermission.checkPermissions(W);
				L.info("Finished Check Permissions, results size",results.length)
				if (results.length) emailStatus = await email.template("watchmanErrors.jade","Watchman Alert => WP Permission:"+W.site,"wpPermission",W,results);
			/*
			// ==> Scan for excess globals

				results = await fme.scanGlobals(sitePath);
			//	L.error("Scan Globals returned",results)		
		*/
			}
			L.info("********************* COMPLETED A CYCLE STARTING AGAIN ***********************")
			go();
	})
}
go();
poll();

	let removeExempts = (results,siteInfo,type) => {
		var final = [];
		var cmp = siteInfo[type].exempt;
		
		for (var j = 0; j < results.length-1; j++) {
				var r = results[j];
				if (cmp.indexOf(r) == -1) final.push(r);
		}
		return final
	}





	